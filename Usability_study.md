# User 1
- They were just looking for something which might show how to find existing recipes
    - They browsed instead of searching
    - There is a lot of information on the landing page
- Curious about orange color?
- Swipes up to start the timer
    - They did not understand the instructions page very clearly
- Navigation instructions ambiguous
    - They thought They had to swipe with both hands
- They stopped the timer by swiping upwards
    - They think start and stop should have the same gesture.
- Easy to navigate the website
- They was able to easily find how to write instructions
    - Found the title easier
    - They wanted to be able to press enter to add the ingredient
    - They wanted to do vulgar fractions instead of decimals in the ingredients container
    - They didn't find it confusing when adding an untimed instruction
        - They seemed to understand that a timer that had 0 in it was a plain instruction.
    - Change add ingredients to _add ingredient_
- They likes the blue labeled instruction
- They liked colour scheme of actual website, but interactive tool They preferred if it wasn't so striking
- Surprised that the timer page was all blue -> thinks that timer pages should be orange but not blue

# User 2
- Searches for the recipe for poor mans fish
    - liked the search a lot
    - liked that it didn't have to press the search button
- Details page isn't labeled details page
    - but They was able to tell from the url
- They thinks the Go Interactive button is very clear
- They thinks the display is weird when it does the instruction
    - Can't tell if it was buggy or overwhelming
- They is **very** close to the screen
    - Should probably say somewhere how far away from the screen they the user should be2
- They likes the navigation at the bottom 
    - It reminds him how to use the timer
- Might be a good thing that it flips for a pretty long time.
    - when They cooks They gets frantic They doesn't like the amount of time it rings for
- They tries to register to create a recipe
    - It's ambiguous where They has to go
    - They makes an account to try and make a recipe when They doesn't need to.
    - It's not immediately obvious where we need to go to make a recipe.
- They likes that it says there is 1 by default in the units.
- They guessed that if there is no timer if They puts in a value of 0.
- They thinks is you swipe down before the timer is completed to swipe down
    - Stop timer should stop the timer regardless if it's started or not.

# User 3

- User was easily able to find the recipe using the search bar
- User entered interactive mode easily
- User thinks they should be able to test the functionality from the instructions page
    - Good idea
- They're a bit confused on how to stop and start timers
- Had difficulty using the gestures.
- They had difficulty finding the list of all recipes
    - Need to change _Browse_ to something more easily recognized
- Recipe creation
    - User easily navigated through all stages necessary to create a valid recipe
    - User understood well that adding timers is optional

# User 4

## Basic navigation
- Luckily found it on the main page as a recommended recipe
- He had some ambiguity understanding that he was in the recipe view
## Interactive mode
- He didn't understand why he's has to "start a timer
- users hands are very close to the screen
- Difficulty using the interactive view.
- Need to describe in better detail what he needs to do.
    - _When the timer finishes we should be able to go to the next instruction_
- Had difficulty closing the screen
    - **Need to make it very clear how far away a user needs to be from the screen**
- It's isn't clear how far away from screen you have to be.
    - they felt it was more natural to move their hand as close to the screen as possible.
## Recipe creation
- The ingredients option needs to change from a plural to a singular
    - Maybe move the + button to the start.

# User 5

## Basic navigation
- Immediately went to the search bar to search for it even though it was in the front page
    - Lieks the autocomplete
- Likes the _most viewed_ recipes segment on the main page
- Understands the simple view of showing the recipe
- Thinking hovering over ingredients it shows more information on the ingredient
    - Or clicking searches or something
- The instructions view would be nice to have some images of some kind
## Interactive mode
- Leaving the tutorial screen is a bit confusing
    - He doesn't know if the tutorial screen is entered in the non-haptic state
- All screens should have a navigation bar
- Not evident that the swipe down function is available to shut down timers.
- Doesn't like that swiping right goes right he likes the turning metaphore
- 
## Recipe creation
- Easily able to find the create new recipe screen
- Immediately entered the name and knew what to do
- Knew how to use the ingredient form without prompting
    - The plus button should be in a different location not at the end of the ingredient form
    - This is because the button seems like you need to _add_ the ingredient to some list when it has actually already added it
- Thinking about having a dropdown of common ingredients
- ROughly half and half people like the way swiping works, so users should be able to choose which way they prefer

# User 6

## Basic navigation
- Immediately used the search bar to find poor mans fish
- Easily found interactive mode
## Interactive mode
- She's not sure what your supposed to do from the interactive page.
    - Had to explain what needed to be done
- Explain why timer duration is so short
- She didn't like that the previous timer went down
- Make the progress bar more visible
- **BUG** Down key not bound to exit
## Recipe creation
- Make the create new recipe button bigger and put it in a more dominant location
- She was able to easily construct a recipe using the form
- Move the plus button to another location
    - Disassociate the plus and minus buttons from the rest of the ingredient/instruction form
    - She thought that the plus button was like another list of ingredients
- Same for instructions and ingredients

# User 7

## Basic navigation
- He went to the search bar immediately
    - Liked auto complete
- Was able to understand the recipe from the default view
- Wants to know the plan about the clickable ingredients
    - Maybe images
- Looks like a tag you can click on.

## Interactive mode
- Went interactive easily enough
- they missed the first step
    - Might be too sensitive
- Navigation boxes could have text describing what the ydo
- Should tell you how to dismiss the timers
- Swipe down should be a "save" feature to remember the previous step you were on.
    - Might be annoying if there are 20 steps and it takes you back to the start.
## Recipe creation
- Immediately added many ingredients then filled them in instead of 1 by 1
- Take us to the recipe when we finish creating it
- The learning curve is too high to understand what to do
    - The tutorial is not informative enough
    - Even adding a label
- Minimum of 2 seconds between moving between slides

# User 8

## Basic navigation

- Search bar to search rather than browse all
- He doesn't like that the ingredient names aren't capitalized
- Questions the need for a _standard recipe view_

## Interactive mode

- Ambiguous about what the timer is
- He doesn't like the hand gestures for navigation
- Increase or choose font size
- Found it really frustrating because he was very close to the screen
- It was very easy for the computer to mis-interpret the gesture.

## Recipe creation

- Color scheme change
- Pictures