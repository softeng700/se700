﻿using System;
using se700.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace se700.Tests
{
    [TestClass]
    public class FractionTests
    {
        [TestMethod]
        public void UnitTests_Fraction_CheckDecimalsReturnVulgar() {
            // Arrange
            string quarterString = "¼";
            string thirdString = "⅓";
            string halfString = "½";
            string twoThirdString = "⅔";
            string threeQuarterString = "¾";

            string quarterCalc, thirdCalc, halfCalc, twoThirdCalc, threeQCalc;
            double quarter = 0.25, third = 0.33, half = 0.5, two3rd = 0.6, threeQ = 0.75;
            // Act
            quarterCalc = Fraction.GetVulgarFraction(quarter);
            thirdCalc = Fraction.GetVulgarFraction(third);
            halfCalc = Fraction.GetVulgarFraction(half);
            twoThirdCalc = Fraction.GetVulgarFraction(two3rd);
            threeQCalc = Fraction.GetVulgarFraction(threeQ);
            //Assert
            Assert.AreEqual(quarterString, quarterCalc);
            Assert.AreEqual(thirdString, thirdCalc);
            Assert.AreEqual(halfString, halfCalc);
            Assert.AreEqual(twoThirdString, twoThirdCalc);
            Assert.AreEqual(threeQuarterString, threeQCalc);
        }

        [TestMethod]
        public void UnitTests_Fraction_CheckApproximateDecimalReturnsVulgar() {
            // Arrange
            string quarterString = "¼";
            string thirdString = "⅓";
            string halfString = "½";
            string twoThirdString = "⅔";
            string threeQuarterString = "¾";
            string oneString = "1";

            string quarterCalc, thirdCalc, halfCalc, twoThirdCalc, threeQCalc, oneCalc;
            double quarter = 0.13, third = 0.28, half = 0.43, two3rd = 0.59, threeQ = 0.69, one = 0.78;
            // Act
            quarterCalc = Fraction.GetVulgarFraction(quarter);
            thirdCalc = Fraction.GetVulgarFraction(third);
            halfCalc = Fraction.GetVulgarFraction(half);
            twoThirdCalc = Fraction.GetVulgarFraction(two3rd);
            threeQCalc = Fraction.GetVulgarFraction(threeQ);
            oneCalc = Fraction.GetVulgarFraction(one);
            //Assert
            Assert.AreEqual(quarterString, quarterCalc);
            Assert.AreEqual(thirdString, thirdCalc);
            Assert.AreEqual(halfString, halfCalc);
            Assert.AreEqual(twoThirdString, twoThirdCalc);
            Assert.AreEqual(threeQuarterString, threeQCalc);
            Assert.AreEqual(oneString, oneCalc);
        }

        [TestMethod]
        public void UnitTests_Fraction_ImproperDecimalToVulgarConversion() {
            // Arrange
            string expectedValue = "1 ¼";
            string actualValue;

            double oneAndQuarter = 1.25;
            // Act
            actualValue = Fraction.GetVulgarFraction(oneAndQuarter);
            // Assert
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
