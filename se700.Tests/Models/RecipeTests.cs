﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using se700.Models.RecipeModels.Ingredients;
using FakeItEasy;
using se700.Models.RecipeModels;

namespace se700.Tests.Models {
    [TestClass]
    public class RecipeTests {
        [TestMethod]
        public void UnitTests_Ingredient_StringFormatsCorrectly_Fraction() {
            // Arrange
            string joe = "Joe";
            string expected = "¼ Cup of Joe.";

            // Act
            IIngredient i = new Ingredient() {
                Quantity = 0.25,
                UnitName = Unit.Cup,
                Name = joe
            };
            string actual = i.GetPlainText();
            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnitTests_Ingredient_StringFormatsCorrectly_Singular() {
            // Arrange
            string joe = "Joe";
            string expected = "1 Cup of Joe.";

            // Act
            IIngredient i = new Ingredient() {
                Quantity = 1,
                UnitName = Unit.Cup,
                Name = joe
            };
            string actual = i.GetPlainText();
            // Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UnitTests_Ingredient_StringFormatsCorrectly() {
            // Arrange
            string joe = "Joe";
            string expected = "2 Cups of Joe.";
            // Act
            IIngredient i = new Ingredient() {
                Quantity = 2,
                UnitName = Unit.Cup,
                Name = joe
            };
            string actual = i.GetPlainText();
            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "A negative value was allowed to" +
            "to be used.")]
        public void UnitTests_Unit_CheckValueNotLessThanZero() {
            // Arrange
            double inappropriateValue = -2.0;
            // Act / Assert
            IIngredient i = new Ingredient();
            i.Quantity = inappropriateValue;
        }

        [TestMethod]
        public void UnitTests_Unit_NoUnitWholeStringFormat() {
            // Arrange
            string expectedValue = "5 Apple.";
            string apple = "Apple";
            string actualValue;
            double input = 5;
            // Act
            IIngredient i = new Ingredient() {
                Quantity = input,
                Name = apple
            };
            actualValue = i.GetPlainText();
            // Assert
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void UnitTests_Unit_NoUnitVulgarStringFormat() {
            // Arrange
            string banana = "banana";
            string expectedValue = "¼ of a banana.";
            string actualValue;
            double input = 0.25;

            // Act
            IIngredient i = new Ingredient() {
                Quantity = input,
                Name = banana
            };
            actualValue = i.GetPlainText();
            // Assert
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void UnitTests_RecipeModel_RenderTextBoxDataIsCorrect() {
            // Arrange
            RecipeModel r = new RecipeModel();
            string expectedValue = "<div class=\"box right top timer-container\"></div>";
            string actualValue;
            // Act
            actualValue = r.RenderTimerBox().ToHtmlString();
            // Assert
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
