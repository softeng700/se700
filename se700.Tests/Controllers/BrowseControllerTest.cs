﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FuzzyString;

namespace se700.Tests.Controllers
{
    [TestClass]
    public class BrowseControllerTest
    {
        [TestMethod]
        public void BrowseController_Search_FuzzySearchExperement()
        {
            // Arrange
            string version1 = "Foo";
            string version2 = "Boo";
            int expected = 1;
        
            // Act
            int actual = version2.LevenshteinDistance(version1);

            // Assert
            Assert.AreEqual<int>(expected, actual);
        }
    }
}
