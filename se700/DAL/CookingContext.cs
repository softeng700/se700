﻿using se700.Models;
using se700.Models.RecipeModels;
using se700.Models.RecipeModels.Ingredients;
using se700.Models.RecipeModels.Step;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace se700.DAL
{
    /// <summary>
    /// Cooking context uses code first entity framework to store user created information
    /// in a database.
    /// 
    /// see: <see cref="System.Data.Entity"/>
    /// </summary>
    public class CookingContext : DbContext
    {
        /// <summary>
        /// Constructor for database layer
        /// </summary>
        public CookingContext() : base("CookingContext") { }
        
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<RecipeModel> Recipes { get; set; }
        public DbSet<PlainInstruction> Instructions { get; set; }
        public DbSet<TimedInstruction> TimedInstructions { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
            modelBuilder.Entity<RecipeModel>().ToTable("Recipes");
            modelBuilder.Entity<Ingredient>().ToTable("Ingredients");
            modelBuilder.Entity<PlainInstruction>().ToTable("Instructions");
            modelBuilder.Entity<TimedInstruction>().ToTable("TimedInstructions");
        }
    }
}