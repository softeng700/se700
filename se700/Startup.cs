﻿using Microsoft.Owin;
using Owin;
using System.Web.Services.Description;

[assembly: OwinStartupAttribute(typeof(se700.Startup))]
namespace se700
{
    public partial class Startup {

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
