﻿var deck = bespoke.from('.cookinator-interactive', [
    bespoke.plugins.touch(),
    bespoke.plugins.state(),
    bespoke.plugins.progress(),
    bespoke.themes.atomantic()
]);


/**
 * A timer object which will call subscribed methods at intervals.
 * @param {number} duration in seconds
 * @param {number} granularity in milliseconds
 */
function Timer(duration, granularity) {
    // Properties
    this.granularity = granularity || 1000;
    this.duration = duration;
    this.running = false;
    this.subscriptions = [];

    // Methods
    this.subscribe = (fn) => {
        if (typeof fn === 'function') {
            this.subscriptions.push(fn);
        } else {
            console.error(fn + " is not a function");
        }
        return this;
    };

    this.expired = () => {
        return !this.running;
    };

    this.start = function () {
        if (this.running) {
            return false;
        }
        this.running = true;

        // Initialize buisness variables
        let start = Date.now();
        let diff = NaN, diffObj;
        // to diferentiate between this Timer object and the "this" countown
        // object
        let parent = this;

        // local method call that starts itself and initates callback after 
        // granularity period

        (function countdown() {
            // calculate the difference since the last countdown
            diff = parent.duration - ((Date.now() - start) / 1000);
            diffObj = Timer.parse(diff);
            // If the difference is greater than 0 there is still time left,
            // wait another granularity segment then call countdown again
            if (diff > 0) {
                setTimeout(countdown, parent.granularity);
            } else {
                // Timer is done
                diff = 0;
                parent.running = false;
            }

            // callback on subscribed functions
            parent.subscriptions.forEach((fn) => {
                fn.call(this, diffObj);
            }, parent);
        }());
    };
}

Timer.parse = (parsable) => {
    return {
        'hours': (parsable / 3600) | 0,
        'minutes': (parsable / 60) % 60 | 0,
        'seconds': (parsable % 60) | 0
    };
}

function TimerManager() {
    this.Timers = [];
    const granularity = 1000;
    this.alarmtone = new Audio('/../../../Content/Audio/alarm.mp3');
    // Annoying set up to get audio to play on chrome.
    this.alarmtone.src = this.alarmtone.src;
    this.alarmtone.pause();
    this.alarmtone.currentTime = 0;

    this.addTimer = ($uiTimer) => {
        // Set up variables
        var time = $uiTimer.data("info");
        var t = new Timer(time, granularity);
        var TM = this;
        var thisSlide = deck.slide();

        // Add a clone of the timer to every slide
        $uiTimer.clone().appendTo($('.timer-container'));
        $('.timer-container div.timer-ready').each((i, e) => {
            // Remove id so we don't break the dom
            $(e).attr("class", "em-green timer timer-active").removeAttr("id");
            // Subscribe to events from the Timer object
            t.subscribe((time) => {
                $(e).text(TimerManager.formatDuration(time));
            });

            t.subscribe(() => {
                if (t.expired()) {
                    TM.alarmtone.play();
                    $(e).attr("class", "em-red timer timer-ringing");
                }
            });
            // Add metadata to the timer depecting which slide it was born of.
            $(e).attr("data-slide", thisSlide);

            // Add click event to slides to navigate to this slide when clicked
            $(e).click(() => {
                // Move to the slide this has been allocated to
                deck.slide(thisSlide);
                // if the timer is ringing and the button is clicked prematurely end the 
                // the ringing and disable the timer
                if ($(e).attr("class") === "em-red timer timer timer-ringing") {
                    TM.alarmtone.pause();
                    TM.alarmtone.currentTime = 0;
                    $(e).attr("class", "em-grey timer timer-disabled");
                }
            });
        });
        // Decorate a selector with only UI timers that are added use this timer.
        $onlyThese = $('.timer-container div.timer-active');

        function updateUI(time) {
            $uiTimer.text(TimerManager.formatDuration(time));
        };

        t.subscribe(updateUI);
        t.subscribe(() => {
            if (t.expired()) {
                TM.alarmtone.play();
                $uiTimer.attr("class", "em-red timer timer-ringing");
            }
        });
        t.start();
        this.Timers.push(t);
    }

    // Find out if there are any active timers before closing interactive mode
    this.areThereActiveTimers = () => {
        var length = this.Timers.length;

        for (var i = 0; i < length; i++) {
            if (!this.Timers[i].expired()) {
                return true;
            }
        }
        return false;
    }
}

TimerManager.formatDuration = (duration) => {
    var dur = duration.hours === 0 ? duration.minutes + ":" + duration.seconds
        : duration.hours + ":" + duration.minutes + ":" + duration.seconds;
    return dur;
}

$(document).ready(() => {
    /**
        On keypress if the key is any of the keys we've selected to be part in navigation then perform these functions

        We also do not want to do to many actions in a small time frame, too much oppertunity for human and machine 
        error. Therefore each function is surrounded by a conditional which times out every 1000ms.
    */
    var TM = new TimerManager();
    // Parse all data passed up to the slides in HH:MM:SS format
    $('div.timer-ready').each((i, e) => {
        $(e).text(TimerManager.formatDuration(Timer.parse($(e).data("info"))));
    });

    let active = true;

    function previous() {
        if (active) {
            deck.prev();
            active = false;
            setTimeout(() => {
                active = true;
            }, 1000);
        }


    }

    function next() {
        if (active) {
            deck.next();
            active = false;
            setTimeout(() => {
                active = true;
            }, 1000);
        }

    }

    function up() {
        if (active) {
            if ($('.bespoke-active').data("bespoke-state") === "Timed") {
                $selectedTimer = $('.bespoke-active div.timer-ready');
                if ($selectedTimer.length) {
                    TM.addTimer($selectedTimer);
                    // Change the class of the timer to the active class.
                    $selectedTimer.attr("class", "em-green timer-active");
                } else if ($('.bespoke-active div.timer-disabled').length){
                    deck.slide($('div.timer-disabled').data('slide'));
                }
            }
            active = false;
            setTimeout(() => {
                active = true;
            }, 1000);
        }


    }

    function down() {
        if (active) {
            if ($('div.timer-ringing').length) {
                // Record the current slide so that we can navigate to it after we change the
                // information on the attribute
                let prevSlide = $('.bespoke-active div.timer-ringing').attr("data-slide");
                $('div.timer-ringing').attr('data-slide', deck.slide());

                deck.slide(prevSlide);
                TM.alarmtone.pause();
                TM.alarmtone.currentTime = 0;
                $('div.timer-ringing').attr("class", "em-grey timer timer-disabled");
            }
            active = false;
            setTimeout(() => {
                active = true;
            }, 1000);
        }

    }

    gest.options.sensitivity(86);
    //gest.options.skinFilter(true);

    gest.options.subscribeWithCallback((gesture) => {
        var prevSlide = deck.slide();
        var lastSlide = document.getElementsByTagName("section").length - 1;

        if (gesture.left) {
            previous();
        } else if (gesture.right) {
            next();
        } else if (gesture.up) {
            up();
        } else if (gesture.down && (prevSlide == lastSlide) && TM.areThereActiveTimers()) {
            down();
        } else if (gesture.down && (prevSlide == lastSlide) && !TM.areThereActiveTimers()) {
            if ($('.timer-container div.timer-ringing').length) {
                down();
            } else {
                window.close();
            }
        } else if (gesture.down) {
            down();
        }
    });
    gest.start();

    $('body').keydown((e) => {
        if (e.which == 37) {
            previous();
        } else if (e.which == 39) {
            next();
        } else if (e.which == 38) {
            up();
        } else if (e.which == 40) {
            down();
        }
    });
});