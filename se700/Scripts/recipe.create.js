﻿const instructionPrompt = "Add a step";
var instructionNo = 0, ingredientNo = 1;
var instructions = [], ingredients = [];
const ingredientPrompt = "Add an ingredient";
var $instructionFocus, $ingredientFocus;

/**
* Instruction is a helper function which will create new Instruction feilds that users
* will be able type instructions into, it will dynamically name each instruction.
**/
var instructioner = () => {
    const s_2 = '<div class="single-instruction-container">';
    const s_3 = '<input class="text-box instruction" id="instruction" type="text" value="' + instructionPrompt + '" />'
        + '<input class="text-box instruction-time" type="number" min="0" value="0" />';
    const s_4 = '<button class="add-instruction" type="button">'
        + '<span class="glyphicon glyphicon-align-center glyphicon-plus-sign"></span>'
        + '</button>'
        + '</div>';
    let returnStr = s_2 + s_3 + s_4;
    instructionNo++;
    return returnStr;
}

var ingredienter = () => {
    const s_1 = '\n<input class="text-box quantity" min="0" type="number" value="1" />' +
        '<select id="Ingredients" name="Ingredients"><option selected="selected" value="None">None</option>' +
        '\n<option value="cup">cup</option><option value="tablespoon">tablespoon</option><option value="teaspoon">teaspoon</option>' +
        '<option value="gram">gram</option><option value="ounce">ounce</option><option value="pound">pound</option>' +
        '<option value="milliliter">milliliter</option></select>' +
        '<input class="text-box single-line ingredient-name" type="text" value="' + ingredientPrompt + '" />' +
        '<button class="add-ingredient" type="button"><span class="glyphicon glyphicon-align-center glyphicon-plus-sign"></span></button>' +
        '</div>';
    let returnstring = '<div class="single-ingredient-container dynamic">' + s_1;
    ingredientNo++;
    return returnstring;
}

$(document).ready(() => {
    // Add tooltips
    $('[data-toggle="tooltip"]').tooltip();

    var sInstruction = '.instruction';
    /**
    * When an instruction field is focused then we need to remove the prompt text and create a new
    * instructions feild to await the next step.
    **/
    $('.instruction-container').on('focus', sInstruction, () => {
        $instructionFocus = $(':focus');
        if ($(':focus').val() === instructionPrompt) {
            $(':focus').val('');
        }
    });

    $('.instruction-container').on('focusout', sInstruction, () => {
        if ($instructionFocus.val() === '') {
            $instructionFocus.val(instructionPrompt);
        }
    });

    // Add instruction container if add button clicked
    $('.instruction-container').on('click', '.add-instruction', () => {
        $(':focus').parent().after(instructioner());
        $(':focus').after('<button class="remove-instruction" type="button">' +
            '<span class="glyphicon glyphicon-align-center glyphicon-minus-sign"></span>' +
            '</button>');
        $(':focus').remove();
    });

    // Remove instruction container if remove button clicked
    $('.instruction-container').on('click', '.remove-instruction', () => {
        $(':focus').parent().remove();
    });

    // If ingredient text is same as prompt, make it blank
    var sIngredient = '.ingredient-name';
    $('.ingredient-container').on('focus', sIngredient, () => {
        $ingredientFocus = $(':focus');
        if ($(':focus').val() === ingredientPrompt) {
            $(':focus').val('');
        }
    });

    // If ingredient text left empty, replace with prompt
    $('.ingredient-container').on('focusout', sIngredient, () => {
        if ($ingredientFocus.val() === '') {
            $ingredientFocus.val(ingredientPrompt);
        }
    });

    // Add ingredient container if add button clicked
    $('.ingredient-container').on('click', '.add-ingredient', () => {
        $(':focus').parent().after(ingredienter());
        $(':focus').after('<button class="remove-ingredient" type="button">' +
            '<span class="glyphicon glyphicon-align-center glyphicon-minus-sign"></span>' +
            '</button>');
        $(':focus').remove();
    });

    // Remove ingredient container if remove button clicked
    $('.ingredient-container').on('click', '.remove-ingredient', () => {
        $(':focus').parent().remove();
    });

    // Suppress enter key to submit
    $('body').keydown((e) => {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('#recipe-form').submit((e) => {
        // Stop the form submitting using the default validation methods
        e.preventDefault();

        if ($("#Name").val() !== '') {
            //Disable submit button to prevent posting to server twice
            $('#submit-button').prop("disabled", true);

            // Grab all the information from the instruction editors
            $('.instruction-container input').each((i, e) => {
                if ($(e).val() === instructionPrompt)
                    return false;
                instructions.push($(e).val());
            });

            // Grab all the ingredients from their counterparts
            $('.single-ingredient-container').each((i, e) => {
                if ($(e).children('.ingredient-name').val() === ingredientPrompt)
                    return false;
                let unit_name = $(e).children('#Ingredients').val();
                let quantity = $(e).children('.text-box.quantity').val();
                let ingredient_name = $(e).children('.ingredient-name').val();
                var ingredient = {
                    'Name': ingredient_name,
                    'Quantity': quantity,
                    'UnitName': unit_name
                }
                ingredients.push(ingredient);
            });

            // Compile dto for transfer to server
            var recipeJSON = {
                "Name": $('#Name').val(),
                "Ingredients": ingredients,
                "Instructions": instructions,
                // Anti forgery token is requried for server to accept this transaction.
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val()
            }

            if (ingredients.length && instructions.length) {
                // Use ajax to transfer object to server
                $.post("Create", recipeJSON, null).done(() => {
                    window.location = "/Recipe/Index"
                });
            }   
        }

    });
});