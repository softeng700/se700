var recipesFound;
/**
 * Javascript for Index.html
 * @constructor
 */
function IndexHelper() {
    
    /**
     * Changes the search criteria to the corresponding value that the 
     * search function is currently set to.
     * @param {string} changeTo 
     */
    this.changeSearch = (changeTo) => {
        // Since we have no logic yet simply change the button text.
        let searchButton = document.getElementById("search-button");

        switch (changeTo) {
            case "By Items":
                // Do some useful change
                break;
            case "Recipe Name":
            // Do some useful change
            default:
                break;
        }
    };
    
    // Add listener to search type list to perform searchbar manipulation when change occurs.
    let stl = document.getElementById("search-type-list"); stl.addEventListener("change", () => {
        this.changeSearch(stl.options[stl.selectedIndex].text);
    });
}

function textChanged() {
    var text = $("#search-input").val();
    var singleRecipe = false;
    var databaseList = [];

    $.getJSON("/Home/RetrieveRecipe", {
        searchQuery: text,
        needSingleRecipe: singleRecipe
    }, (data) => {
        if (data != null) {
            recipesFound = data;
            
            // Populate autocomplete list with names of recipes
            for (var i = 0; i < data.length; i++) {
                databaseList.push(data[i].Name);
            }

            $("#search-input").autocomplete({
                source: databaseList,
                autoFocus: true,
                appendTo: '#menu-container',
                select: (e, ui) => {
                    var recipe = ui.item.value;
                    $("#search-input").val(recipe);
                    submitSearch(e, recipe, true);
                }
            });
        }
    });
}

function submitSearch(e, recipeName, singleRecipe) {
    if ($("#value-check").val() === "false") {
        e.preventDefault();

        // Retrieves single recipe that was searched for
        $.getJSON("/Home/RetrieveRecipe", {
            searchQuery: recipeName,
            needSingleRecipe: singleRecipe
        }, (data) => {
            if (data != null) {

                // Set the hidden id value to the recipe's id for retrieval
                var recipeId = data.ID;
                $("#id").val(recipeId);

            }
        }).done(() => {

            // Once the value has been updated, resubmit the form
            $("#value-check").val("true");
            $("#search-form").submit();
        });
    } else {
        $("#value-check").val("false");
    }
}

$(document).ready(() => {
    $("#search-input").focus(() => {
        if ($("#search-input").val() === "Search for something tasty") {
            $("#search-input").val("");
        }
        $("#search-button").css("background", "#ff8604");
    });
    $("#search-input").focusout(() => {
        $("#search-button").css("background", "#04BCFF");
        if ($("#search-input").val() === "") {
            $("#search-input").val("Search for something tasty");
        }
    });

    $("#search-type-list").change(() => {
        if ($("#search-type-list").val() === "Parse"){
            $(".recipe-search").hide();
            $(".recipe-parse").show();
        } else {
            $(".recipe-search").show();
            $(".recipe-parse").hide();
        }
    });

    // Whenever the user has pressed a key, search for recipes that match
    $("#search-input").on('input', textChanged);

    // Whenever the user clicks submit/enter on the recipe search
    $("#search-form").one('submit', (e) => {
        submitSearch(e, $("#search-input").val(), true);
    });
});