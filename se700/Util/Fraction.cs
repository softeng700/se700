﻿using System;

namespace se700.Util
{
    /// <summary>
    /// Utilit class to create unicode vulgar fractions.
    /// 
    /// Shamelessly stolen from stackoverflow:
    /// <see cref="https://stackoverflow.com/questions/25016191/simplifying-fraction-to-vulgar-fraction-code-in-c-sharp#25016487"/>
    /// </summary>
    public static class Fraction
    {

        public static string GetVulgarFraction(double value) {
            if (value < 0) {
                return string.Format("-{0}", GetVulgarFraction(value));
            }

            if (value > 1 && (value - (int)value != 0)) {
                return string.Format("{0} {1}", (int)value, GetVulgarFraction(value - (int)value));

            }

            if (value <= 0.75 && value > 0.6667) return "¾";
            if (value <= 0.6667 && value > 0.5) return "⅔";
            if (value <= 0.5 && value > 0.3333) return "½";
            if (value <= 0.3333 && value > 0.25) return "⅓";
            if (value <= 0.25 && value > 0) return "¼";

            return (Math.Round(value).ToString());
        }

        public static string GetVulgarFraction(int numerator, int denominator) {
            if (numerator < 0) {
                // Handle -1/2 as "-½"
                return string.Format("-{0}",
                    GetVulgarFraction(-numerator, denominator));
            }
            if (numerator > denominator) {
                // Handle 7/4 as "1 ¾"
                return string.Format("{0} {1}",
                    numerator / denominator,
                    GetVulgarFraction(numerator % denominator, denominator));
            }
            // Handle 0/1 = "0"
            if (numerator == 0) return "0";
            // Handle 10/1 = "10"
            if (denominator == 1) return numerator.ToString();
            // Handle 1/2 = ½
            if (denominator == 2) {
                if (numerator == 1) return "½";
            }
            // Handle 1/4 = ¼
            if (denominator == 4) {
                if (numerator == 1) return "¼";
                if (numerator == 3) return "¾";
            }
            // Handle 1/8 = ⅛
            if (denominator == 8) {
                if (numerator == 1) return "⅛";
                if (numerator == 3) return "⅜";
                if (numerator == 5) return "⅝";
                if (numerator == 7) return "⅞";
            }
            // Catch all
            return string.Format("{0}/{1}", numerator, denominator);
        }
    }
}