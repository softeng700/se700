﻿using se700.Models.RecipeModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace se700.Models
{
    /// <summary>
    /// Primary model managing the Index view
    /// </summary>
    public class HomeViewModel {
        public string SearchQuery { get; set; }
        public List<IRecipeModel> FrontPage { get; set; }
    }
}