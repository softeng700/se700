﻿using System;
using System.Collections.Generic;
using se700.Models.RecipeModels;
using se700.Models.RecipeModels.Ingredients;
using se700.Models.RecipeModels.Step;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;


namespace se700.Models.RecipeModels {
    /// <summary>
    /// Plain Old Component Object (POCO) modeling recipe information
    /// </summary>
    public class RecipeModel : IRecipeModel {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [UIHint("Recipe")]
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [UIHint("Instructions")]
        public virtual List<AbstractInstruction> Instructions { get; set; }
        public virtual ICollection<Ingredient> Ingredients { get; set; }

        protected int _timerID = 0;

        /// <summary>
        /// Default constructor instansiating list properties
        /// </summary>
        public RecipeModel() {
            Instructions = new List<AbstractInstruction>();
            Ingredients = new HashSet<Ingredient>();
        }

        /// <summary>
        /// Model call that will create a timer box by returning an MvCHtmlString that can be
        /// used in razor views.
        /// </summary>
        /// <returns>MVCHtmlString representing a timer box</returns>
        public IHtmlString RenderTimerBox() {
            var box = new TagBuilder("div");
            box.MergeAttribute("class", "box right top timer-container");
            return MvcHtmlString.Create(box.ToString());
        }

        /// <summary>
        /// Create a timer box in a non default location.
        /// </summary>
        /// <param name="location">Location on the screen where the box will be rendered</param>
        /// <returns>MVCHtmlString representing a custom timer box</returns>
        public IHtmlString RenderTimerBox(params string[] location)
        {
            var box = new TagBuilder("div");
            box.MergeAttribute("class", "box timer-container " + string.Join(" ", location));
            return MvcHtmlString.Create(box.ToString());
        }

        /// <summary>
        /// Create a timer that will be rendered in the view that can be operated on with JavaScript.
        /// </summary>
        /// <param name="instruction">The instruction that this timer is representing</param>
        /// <returns>MVCHtmlString that represents a timer</returns>
        public IHtmlString RenderTimer(TimedInstruction instruction) {
            var timer = new TagBuilder("div");
            timer.MergeAttribute("class", "em-green timer timer-ready");
            timer.MergeAttribute("data-info", instruction.Time.ToString());
            timer.MergeAttribute("data-slide", "0");
            timer.MergeAttribute("id", "timer-" + _timerID++);
            timer.InnerHtml = instruction.Time.ToString();
            return MvcHtmlString.Create(timer.ToString());
        }

        /// <summary>
        /// Create the recipe name as an HTML element to be added to the view as Razor.
        /// </summary>
        /// <returns>MVCHtmlString representing the recipe name</returns>
        public IHtmlString RenderRecipeName()
        {
            var recipeNameObject = new TagBuilder("div");
            recipeNameObject.MergeAttribute("class", "box top left");
            var recipeNameText = new TagBuilder("h1");
            recipeNameText.MergeAttribute("class", "recipe-name");
            recipeNameText.InnerHtml = this.Name;
            recipeNameObject.InnerHtml = recipeNameText.ToString();
            return MvcHtmlString.Create(recipeNameObject.ToString());
        }
    }
}