﻿using se700.Models.RecipeModels.Ingredients;
using se700.Models.RecipeModels.Step;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se700.Models.RecipeModels
{
    public interface IRecipeModel
    {
        int ID { get; set; }
        string Name { get; set; }

        List<AbstractInstruction> Instructions { get; set; }
        ICollection<Ingredient> Ingredients { get; set; }
    }
}
