﻿using se700.Models.RecipeModels.Ingredients;
using System.Collections.Generic;

namespace se700.Models.RecipeModels {
    /// <summary>
    ///  Data transfer object created at the front end and passed to the backend.
    ///  This was created to abstract the complex structure of the objects in JavaScript
    ///  to allow the front end to only have to handle text.
    ///  
    /// Conversion of the DTO into a realized object is done in the controller.
    /// </summary>
    public class RecipeModelDTO {
        public string Name { get; set; }
        public IList<string> Instructions { get; set; }
        public ICollection<Ingredient> Ingredients { get; set; }
    }
}