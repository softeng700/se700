﻿using System;
using System.Linq;
using se700.Models.RecipeModels.Ingredients;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using se700.Models.RecipeModels.Step;
using System.ComponentModel.DataAnnotations;
using se700.Util;
using System.Web.Mvc;

namespace se700.Models.RecipeModels.Ingredients
{
    /// <summary>
    /// Implentation of an Ingredient object 
    /// </summary>
    public class Ingredient : IIngredient {
        #region Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public Unit UnitName { get; set; }
        private double _quantity;
        public double Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Value cannot be less than zero");
                _quantity = value;
            }
        }
        #endregion

        #region Db
        public virtual AbstractInstruction Instructions {get; set;}
        public virtual ICollection<RecipeModel> Recipe { get; set; }
        #endregion

        public Ingredient() {
            // Initialize enum to None value
            UnitName = Unit.None;
        }

        #region Methods
        /// <summary>
        /// Get the plain text representation of this ingredient.
        /// </summary>
        /// <returns>String representing ingredient</returns>
        public string GetPlainText() {
            return string.Format("{0} {1}", getUnitText(), Name);
        }

        /// <summary>
        /// Use the Fraction utlitiy class to create a vulgar unicode fraction
        /// of a decimal number to the nearest vulgar fraction.
        /// </summary>
        /// <returns>A string representing the vulgar fraction</returns>
        private string getUnitText() {
            if (this.Quantity == 0)
                return "";
            string returnString;
            if (this.Quantity < 1) {
                if (this.UnitName == Unit.None)
                    return string.Format("{0} of a", Fraction.GetVulgarFraction(this.Quantity));
                returnString = string.Format("{0} {1} of", Fraction.GetVulgarFraction(this.Quantity), this.UnitName.ToString());
            } else {
                if (this.UnitName == Unit.None)
                    return string.Format("{0}", Fraction.GetVulgarFraction(this.Quantity));
                // append "s" for multiples.
                if (Quantity > 1 && this.UnitName != Unit.None) {
                    returnString = string.Format("{0} {1}{2} of", Fraction.GetVulgarFraction(this.Quantity), this.UnitName.ToString(), "s");
                } else {
                    returnString = string.Format("{0} {1} of", Fraction.GetVulgarFraction(this.Quantity), this.UnitName.ToString());
                }
            }
            return returnString.Trim();
        }

        /// <summary>
        /// Creates a collection of SelectListItems to be added to a dropdown list for the front
        /// end.
        /// 
        /// Values are extracted from the Unit Enum so new search options can be added later.
        /// </summary>
        /// <returns>Collection of dropdown list items.</returns>
        public static IEnumerable<SelectListItem> GetSearchOptions() {
            bool first = true;
            foreach (var so in Enum.GetValues(typeof(Unit))) {
                Unit _so = (Unit)so;
                var @return = new SelectListItem() {
                    Text = _so.ToString(),
                    Value = so.ToString()
                };
                if (first == true) {
                    @return.Selected = true;
                    first = false;
                }
                yield return @return;
            }
        }
        #endregion
    }
}