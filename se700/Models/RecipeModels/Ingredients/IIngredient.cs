﻿using se700.Models.RecipeModels.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace se700.Models.RecipeModels.Ingredients {
    /// <summary>
    /// Interface representing an ingredient object in our domain.
    /// A POCO.
    /// </summary>
    public interface IIngredient {
        /// <summary>
        /// Unique ID.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// The name of the ingredient.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Get a human readable representation of this object.
        /// </summary>
        /// <returns>String describing this ingredient.</returns>
        string GetPlainText();

        /// <summary>
        /// Name of this unit
        /// </summary>
        Unit UnitName { get; set; }

        /// <summary>
        /// The quantity of this ingredient.
        /// </summary>
        double Quantity { get; set; }
    }

    public enum Unit {
        None, cup, tablespoon, teaspoon, gram, ounce, pound, milliliter
    }
}
