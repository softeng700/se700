﻿using se700.Models.RecipeModels.Ingredients;
using System.Collections.Generic;
using System.Linq;

namespace se700.Models.RecipeModels.Step {
    /// <summary>
    /// IInstruction dictates the properties and methods needed to model a recipe instruction.
    /// </summary>
    public interface IInstruction {
        int ID { get; set; }
        string Description { get; set; }
        ICollection<IIngredient> Ingredients { get; set; }
        RecipeModel Recipe { get; set; }
    }

}