﻿using System.Collections.Generic;
using se700.Models.RecipeModels.Ingredients;
using System.ComponentModel.DataAnnotations.Schema;

namespace se700.Models.RecipeModels.Step
{
    public abstract class AbstractInstruction : IInstruction
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Description { get; set; }

        public virtual ICollection<IIngredient> Ingredients { get; set; }
        public virtual RecipeModel Recipe { get; set; }
    }
}