﻿using System;
using System.Collections.Generic;
using se700.Models.RecipeModels.Ingredients;

namespace se700.Models.RecipeModels.Step
{
    /// <summary>
    /// Plain instruction, as it is now, is just a concretion of the Abstract instruction
    /// methods and properties.
    /// </summary>
    public class PlainInstruction : AbstractInstruction
    {
    }
}