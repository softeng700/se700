﻿using System;
using System.Collections.Generic;
using se700.Models.RecipeModels.Ingredients;

namespace se700.Models.RecipeModels.Step
{
    /// <summary>
    ///  A timed instruction is identical to a plain instruction in all regards except that it 
    ///  also contains a property for time which will be used to time instructions for the user.
    /// </summary>
    public class TimedInstruction : PlainInstruction {
        /// <summary>
        /// The time it will take to complete this step in seconds.
        /// </summary>
        public int Time { get; set; }
    }
}