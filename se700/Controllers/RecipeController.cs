﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using se700.DAL;
using se700.Models.RecipeModels;
using se700.Models.RecipeModels.Step;

namespace se700.Controllers {
    /// <summary>
    /// Class responsible for all the views involved with recipes
    /// </summary>
    public class RecipeController : Controller {
        // Recipe model information from database context
        private CookingContext db = new CookingContext();

        // GET: Recipe
        /// <summary>
        /// Index page for recipes, gets and lists all recipes in a list.
        /// </summary>
        /// <returns>ActionResult of recipepage.</returns>
        public ActionResult Index() {
            return View(db.Recipes.ToList());
        }

        // GET: Recipe/Details/5
        /// <summary>
        /// Gets the details for one specific recipe.
        /// </summary>
        /// <param name="id">The UUID of a recipe</param>
        /// <returns>The view specific view related to this recipe</returns>
        public ActionResult Details(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipeModel recipeModel = db.Recipes.Find(id);
            if (recipeModel == null) {
                return HttpNotFound();
            }
            return View(recipeModel);
        }

        // GET: Recipe/Create
        /// <summary>
        /// Loads the create a recipe page.
        /// 
        /// This view is where users will go to create new recipes to be added
        /// to the system.
        /// </summary>
        /// <returns>View showing the recipe creation screen</returns>
        public ActionResult Create() {
            return View();
        }

        // POST: Recipe/Create
        /// <summary>
        /// POST call to database with recipe information from the recipe creation form
        /// 
        /// ID has been removed from this POSTback since we want the system to generate the ID
        /// to prevent malicious posting to existing entities.
        /// </summary>
        /// <param name="recipeModelDTO">Data transfer object representing the recipe</param>
        /// <returns>Redirection is handled by javascript</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Ingredients,Instructions")] RecipeModelDTO recipeModelDTO) {
            RecipeModel recipeModel = new RecipeModel();
            if (ModelState.IsValid) {
                recipeModel = new RecipeModel {
                    Name = recipeModelDTO.Name,
                    Ingredients = recipeModelDTO.Ingredients
                };
                // Pass up the timed/plain instructions
                if (recipeModelDTO.Instructions == null)
                    return new HttpNotFoundResult();

                for (int i = 0; i < recipeModelDTO.Instructions.Count; i = i + 2) {
                    if (recipeModelDTO.Instructions[i + 1] == "0") {
                        recipeModel.Instructions.Add(new PlainInstruction() {
                            Description = recipeModelDTO.Instructions[i],
                        });
                    } else {
                        recipeModel.Instructions.Add(new TimedInstruction() {
                            Description = recipeModelDTO.Instructions[i],
                            Time = (int) (double.Parse(recipeModelDTO.Instructions[i + 1]) * 60)
                        });
                    }
                }
                db.Recipes.Add(recipeModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(recipeModel);
        }

        // GET: Recipe/Delete/5
        /// <summary>
        /// Returns the deletion view where the user asked to confirm if they want to delete the
        /// recipe.
        /// </summary>
        /// <param name="id">The model that is set to be deleted</param>
        /// <returns>The recipe deletion page</returns>
        public ActionResult Delete(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipeModel recipeModel = db.Recipes.Find(id);
            if (recipeModel == null) {
                return HttpNotFound();
            }
            return View(recipeModel);
        }

        // POST: Recipe/Delete/5
        /// <summary>
        /// Performs the deletion of an entity from the database.
        /// </summary>
        /// <param name="id">The id of the model that will be deleted</param>
        /// <returns>Recipe Index page</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) {
            RecipeModel recipeModel = db.Recipes.Find(id);
            IEnumerable<AbstractInstruction> abstractInstructions = db.Recipes.Find(id).Instructions;
            foreach (var ins in abstractInstructions.ToList())
                db.Instructions.Remove(ins as PlainInstruction);
            db.Recipes.Remove(recipeModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Interactive(int? id) {
            if (id == null) {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RecipeModel recipeModel = db.Recipes.Find(id);
            if (recipeModel == null) {
                return HttpNotFound();
            }
            return View(recipeModel);
        }
    }
}
