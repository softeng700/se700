﻿using se700.DAL;
using se700.Models;
using se700.Models.RecipeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using se700.Util;
using Newtonsoft.Json;

namespace se700.Controllers {
    public class HomeController : Controller {
        private CookingContext db = new CookingContext();


        [HttpGet]
        /// <summary>
        /// <para>RetrieveRecipe is used to retrieve either one or multiple recipes for use in updating autocomplete or when searching for a recipe</para>
        /// <param name="searchQuery">The name of the recipe we are looking for.</param>
        /// <param name="needSingleRecipe">To determine if we are retrieving a single recipe or not.</param>
        /// </summary>
        public ActionResult RetrieveRecipe(string searchQuery, Boolean needSingleRecipe) {
            List<RecipeModel> recipesToReturn = new List<RecipeModel>();
            // Instantiate list of recipes from db
            List<RecipeModel> recipes = db.Recipes.ToList();
            if (!needSingleRecipe) {
                foreach (RecipeModel recipe in recipes) {
                    // Perform fuzzy search
                    int difference = FuzzySearch.Compute(searchQuery, recipe.Name);

                    // Add to recipesToReturn if it matches fuzzy search criteria
                    if (difference < 20) {
                        recipesToReturn.Add(recipe);
                    }
                }

                // Serializing the list of recipes into a JSON object
                var list = JsonConvert.SerializeObject(recipesToReturn,
                    Formatting.None,
                    new JsonSerializerSettings() {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });

                return Content(list, "application/json");
            } else {
                foreach (RecipeModel recipe in recipes) {
                    if (String.Equals(recipe.Name, searchQuery, StringComparison.OrdinalIgnoreCase)) {

                        string recipeInJson = JsonConvert.SerializeObject(recipe,
                                        Formatting.None,
                                        new JsonSerializerSettings() {
                                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                        });
                        return Content(recipeInJson, "application/json");
                    }
                }
                return HttpNotFound();
            }

        }

        /// <summary>
        /// <para>Used to retrieve 3 unique recipes for the front page.</para>
        /// </summary>
        /// <param name="recipeList">Database of recipes to go through.</param> 
        /// <param name="dbLength">Used to determine number for boolean array.</param>
        /// <returns>List of recipes of length 3 to show on the front page.</returns>
        public List<IRecipeModel> DisplayRecipes(RecipeModel[] recipeList, int dbLength)
        {
            var frontPage = new List<IRecipeModel>();
            bool[] isDone = new bool[dbLength];
            var rand = new Random();

            while (frontPage.Count != dbLength)
            {
                int val = rand.Next(0, dbLength);
                if (!isDone[val])
                {
                    frontPage.Add(recipeList[val]);
                    isDone[val] = true;
                }
            }

            return frontPage;
        }

        /// <summary>
        /// Action controlling the main page of our application.
        /// </summary>
        /// <returns>View Result abstracted as an action result.</returns>
        public ActionResult Index() {
            var data = db.Recipes.ToArray();
            var m = new HomeViewModel() {
                SearchQuery = "Search for something tasty"
            };
            if (db.Recipes.Count() > 0)
            {
                var frontPage = DisplayRecipes(data, data.Length);
                m.FrontPage = frontPage;
            }

            return View(m);
        }

        /// <summary>
        /// Action rendering the about page
        /// </summary>
        /// <returns>About page implementation</returns>
        public ActionResult About() {
            ViewBag.Message = "Welcome to Cookinator. Your worry-free recipe guide.";

            return View();
        }

        /// <summary>
        /// Action method which navigates to the RecipeView and delegates control to the recipecontroller.
        /// </summary>
        /// <returns>ViewResult representing</returns>
        public ActionResult Recipe() {
            ViewBag.Message = "Your current recipe.";

            return View();
        }

        public ActionResult QuickUse() {
            ViewBag.Message = "How to use the application";

            return View();
        }
    }
}