# Digitization of Cooking Using Advanced HCI

## Description
Cooking from a recipe, either from a website or a piece of paper, can be an arduous task if the cook has difficulty remembering the steps in the process. This is disruptive to the cooking and can often extend the time it takes to complete the recipe.
The aim of this project is to provide an interface that will give the users a non-haptic experience while cooking a recipe. Using their tablet or laptop, the user will be able to go through the steps of a recipe using non-touch methods.

## Outcome
- SQL database containing recipes
- Web application service application
- Front end web interface for desktop - With a thick or thin client for mobile.

## Specifics
- Language: C# (Razor), JavaScript, CSS
- IDE: Visual Studio
- Framework: ASP.net MVC 5
- Version Control: Bitbucket
- Issue tracking: Trello

## Installation
To demo this application we will be running it in debug mode and therefore this is the only mode of operation that has been tested. However, should the reader chose so they 
are able to _Publish_ this application so that it cajn run on an IIS server however that is not the method of operation that will be shown here

1. Ensure that you have _Visual Studio 2015_ patch 3 or higher installed on your computer
2. Download the repository
3. Launch the application either from the main folder by clicking on se700.sln or from visual studio from using the _Open project_ dialog
4. Once visual studio has loaded the application you should be able to run the solution in debug mode and it will open in your default browser
	- The program has only been tested in chrome
5. Should the application not launch do a rebuild of the project then launch again.

